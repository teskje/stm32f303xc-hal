//! Embedded flash memory

use crate::pac;

/// Extension trait to constrain the `FLASH` peripheral
pub trait FlashExt {
    /// Constrain the `FLASH` peripheral to play nicely with the other
    /// abstractions
    fn constrain(self) -> Flash;
}

impl FlashExt for pac::FLASH {
    fn constrain(self) -> Flash {
        Flash {
            acr: ACR { _private: () },
        }
    }
}

/// Constrained `FLASH` peripheral
pub struct Flash {
    /// Access control register (`FLASH_ACR`)
    pub acr: ACR,
}

/// Flash access control register (`FLASH_ACR`)
pub struct ACR {
    _private: (),
}

impl ACR {
    pub(crate) fn acr(&mut self) -> &pac::flash::ACR {
        // NOTE(unsafe) This proxy grants exclusive access to this register
        unsafe { &(*pac::FLASH::ptr()).acr }
    }
}
