//! Serial peripheral interface (SPI)

use crate::{
    dma::{self, dma1, dma2},
    gpio::{
        gpioa::{PA5, PA6, PA7},
        gpiob::{PB13, PB14, PB15, PB3, PB4, PB5},
        gpioc::{PC10, PC11, PC12},
        gpiof::{PF10, PF9},
        AF5, AF6,
    },
    pac::{self, spi1::cr1::BR_A, SPI1, SPI2, SPI3},
    rcc::{Bus, Clocks, Enable, Reset},
    time::Hertz,
};
use as_slice::AsMutSlice;
use core::{
    ops::Deref,
    sync::atomic::{self, Ordering},
};

/// An enabled SPI
pub struct Spi<SPI>(SPI);

impl<SPI> Spi<SPI>
where
    SPI: Deref<Target = pac::spi1::RegisterBlock> + Enable + Reset,
{
    fn init<SCK, MISO, MOSI, F>(
        spi: SPI,
        pins: (SCK, MISO, MOSI),
        freq: F,
        clocks: Clocks,
        apb: &mut SPI::Bus,
    ) -> Self
    where
        SCK: Sck<SPI>,
        MISO: Miso<SPI>,
        MOSI: Mosi<SPI>,
        F: Into<Hertz>,
    {
        // ensure the pins cannot be used for anything else
        drop(pins);

        SPI::enable(apb);
        SPI::reset(apb);

        let br = Self::calc_baud_rate(clocks, freq.into());

        spi.cr2.write(|w| w.frxth().quarter());

        spi.cr1.write(|w| {
            w.cpha().first_edge();
            w.cpol().idle_low();
            w.lsbfirst().lsbfirst();

            w.br().variant(br);

            // enable software slave management (frees the NSS pin)
            w.ssm().enabled();

            // configure master mode
            w.mstr().master();
            w.ssi().slave_not_selected();

            w.bidimode().unidirectional();
            w.rxonly().full_duplex();

            w.crcen().disabled();
            w.spe().enabled()
        });

        Spi(spi)
    }

    fn calc_baud_rate(clocks: Clocks, freq: Hertz) -> BR_A {
        let pclk = SPI::Bus::clock(clocks).0;
        let freq = freq.0;
        assert!(pclk % freq == 0);

        match pclk / freq {
            2 => BR_A::DIV2,
            4 => BR_A::DIV4,
            8 => BR_A::DIV8,
            16 => BR_A::DIV16,
            32 => BR_A::DIV32,
            64 => BR_A::DIV64,
            128 => BR_A::DIV128,
            256 => BR_A::DIV256,
            _ => panic!("invalid BR"),
        }
    }
}

macro_rules! spi {
    ( $SPIx:ty, $spix:ident ) => {
        impl Spi<$SPIx> {
            /// Initialize an SPI
            pub fn $spix<SCK, MISO, MOSI, F>(
                spi: $SPIx,
                pins: (SCK, MISO, MOSI),
                freq: F,
                clocks: Clocks,
                apb: &mut <$SPIx as Enable>::Bus,
            ) -> Self
            where
                SCK: Sck<$SPIx>,
                MISO: Miso<$SPIx>,
                MOSI: Mosi<$SPIx>,
                F: Into<Hertz>,
            {
                Self::init(spi, pins, freq, clocks, apb)
            }
        }
    };
}

spi!(SPI1, spi1);
spi!(SPI2, spi2);
spi!(SPI3, spi3);

/// Marker trait for pins than can be used as SCK pin
pub trait Sck<SPI> {}
/// Marker trait for pins than can be used as MISO pin
pub trait Miso<SPI> {}
/// Marker trait for pins than can be used as MOSI pin
pub trait Mosi<SPI> {}

impl Sck<SPI1> for PA5<AF5> {}
impl Miso<SPI1> for PA6<AF5> {}
impl Mosi<SPI1> for PA7<AF5> {}

impl Sck<SPI1> for PB3<AF5> {}
impl Miso<SPI1> for PB4<AF5> {}
impl Mosi<SPI1> for PB5<AF5> {}

impl Sck<SPI2> for PB13<AF5> {}
impl Miso<SPI2> for PB14<AF5> {}
impl Mosi<SPI2> for PB15<AF5> {}

impl Sck<SPI2> for PF9<AF5> {}
impl Sck<SPI2> for PF10<AF5> {}

impl Sck<SPI3> for PB3<AF6> {}
impl Miso<SPI3> for PB4<AF6> {}
impl Mosi<SPI3> for PB5<AF6> {}

impl Sck<SPI3> for PC10<AF6> {}
impl Miso<SPI3> for PC11<AF6> {}
impl Mosi<SPI3> for PC12<AF6> {}

/// DMA for transmission on an SPI
pub struct SpiTxDma<SPI, C> {
    spi: Spi<SPI>,
    channel: C,
}

macro_rules! spi_tx_dma {
    ( $SPIx:ty => $chan:ty ) => {
        impl Spi<$SPIx> {
            /// Enable DMA mode for this ADC
            pub fn into_tx_dma(self, channel: $chan) -> SpiTxDma<$SPIx, $chan> {
                SpiTxDma { spi: self, channel }
            }
        }

        impl SpiTxDma<$SPIx, $chan> {
            /// Start writing to the SPI via DMA
            pub fn start<B>(mut self, buf: &'static mut B) -> dma::Transfer<B, $chan, Spi<$SPIx>>
            where
                B: AsMutSlice<Element = u8>,
            {
                let slice = buf.as_mut_slice();
                self.configure_channel(slice);

                atomic::compiler_fence(Ordering::SeqCst);

                self.channel.start_write();
                self.spi.0.cr2.modify(|_, w| w.txdmaen().enabled());

                dma::Transfer::new(buf, self.channel, self.spi)
            }

            fn configure_channel(&mut self, slice: &mut [u8]) {
                use dma::{Increment, WordSize};

                let pa = &self.spi.0.dr as *const _ as u32;
                let ma = slice.as_ptr() as u32;
                self.channel.set_peripheral_address(pa, Increment::Disable);
                self.channel.set_memory_address(ma, Increment::Enable);

                self.channel.set_transfer_length(slice.len());
                self.channel.set_word_size(WordSize::Bits8);
            }
        }

        impl dma::TransferPayload for Spi<$SPIx> {
            fn stop(&mut self) {
                self.0.cr2.modify(|_, w| w.txdmaen().disabled());
            }
        }
    };
}

spi_tx_dma!(SPI1 => dma1::C3);
spi_tx_dma!(SPI2 => dma1::C5);
spi_tx_dma!(SPI3 => dma2::C2);
