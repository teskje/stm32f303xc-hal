//! HAL for the STM32F303xC microcontroller models

#![no_std]
#![warn(missing_docs)]
#![warn(rust_2018_idioms)]

pub mod adc;
pub mod dma;
pub mod flash;
pub mod gpio;
pub mod prelude;
pub mod rcc;
pub mod spi;
pub mod time;
pub mod timer;

pub use cortex_m as core;
pub use stm32f3::stm32f303 as pac;
