//! Reset and clock control (RCC)

use crate::{
    flash::ACR,
    pac::{
        self,
        rcc::cfgr::{HPRE_A, PLLSRC_A, PPRE1_A, PPRE2_A},
    },
    time::Hertz,
};

/// Extension trait to constrain the `RCC` peripheral
pub trait RccExt {
    /// Constrain the `RCC` peripheral to play nicely with the other
    /// abstractions
    fn constrain(self) -> Rcc;
}

impl RccExt for pac::RCC {
    fn constrain(self) -> Rcc {
        Rcc {
            cfgr: CFGR {
                hse: None,
                sysclk: None,
                hclk: None,
                pclk1: None,
                pclk2: None,
            },
            ahb: AHB { _private: () },
            apb1: APB1 { _private: () },
            apb2: APB2 { _private: () },
        }
    }
}

/// Constrained `RCC` peripheral
pub struct Rcc {
    /// Clock configuration register (`RCC_CFGR`)
    pub cfgr: CFGR,
    /// AMBA High-performance Bus (AHB) registers
    pub ahb: AHB,
    /// Advanced Peripheral Bus 1 (APB1) registers
    pub apb1: APB1,
    /// Advanced Peripheral Bus 2 (APB2) registers
    pub apb2: APB2,
}

const HSI: u32 = 8_000_000; // Hz

/// RCC clock configuration register (`RCC_CFGR`)
pub struct CFGR {
    hse: Option<u32>,
    sysclk: Option<u32>,
    hclk: Option<u32>,
    pclk1: Option<u32>,
    pclk2: Option<u32>,
}

impl CFGR {
    /// Use HSE (external oscillator) instead of HSI (internal RC oscillator)
    /// as the clock source.
    ///
    /// This will result in a hang if a HSE is not connected or fails to start.
    pub fn use_hse<F>(mut self, freq: F) -> Self
    where
        F: Into<Hertz>,
    {
        self.hse = Some(freq.into().0);
        self
    }

    /// Set `SYSCLK` frequency
    pub fn sysclk<F>(mut self, freq: F) -> Self
    where
        F: Into<Hertz>,
    {
        self.sysclk = Some(freq.into().0);
        self
    }

    /// Set `HCLK` frequency
    pub fn hclk<F>(mut self, freq: F) -> Self
    where
        F: Into<Hertz>,
    {
        self.hclk = Some(freq.into().0);
        self
    }

    /// Set `PCLK1` frequency
    pub fn pclk1<F>(mut self, freq: F) -> Self
    where
        F: Into<Hertz>,
    {
        self.pclk1 = Some(freq.into().0);
        self
    }

    /// Set `PCLK2` frequency
    pub fn pclk2<F>(mut self, freq: F) -> Self
    where
        F: Into<Hertz>,
    {
        self.pclk2 = Some(freq.into().0);
        self
    }

    /// Freeze and apply the clock configuration
    pub fn freeze(self, acr: &mut ACR) -> Clocks {
        let (sysclk, pllmul, pllsrc) = self.calc_sysclk();

        let hclk = self.hclk.unwrap_or(sysclk);
        let pclk1 = self.pclk1.unwrap_or(hclk);
        let pclk2 = self.pclk2.unwrap_or(hclk);

        assert!(sysclk <= 72_000_000);
        assert!(hclk <= 72_000_000);
        assert!(pclk1 <= 36_000_000);
        assert!(pclk2 <= 72_000_000);

        let hpre = calc_hpre(sysclk, hclk);
        let ppre1 = calc_ppre1(hclk, pclk1);
        let ppre2 = calc_ppre2(hclk, pclk2);

        set_flash_latency(sysclk, acr);

        let rcc = unsafe { &*pac::RCC::ptr() };

        if self.hse.is_some() {
            // enable HSE
            rcc.cr.modify(|_, w| w.hseon().set_bit());
            while rcc.cr.read().hserdy().bit_is_clear() {}
        }

        // enable PLL
        rcc.cfgr.modify(|_, w| {
            w.pllsrc().variant(pllsrc);
            w.pllmul().bits(pllmul)
        });
        rcc.cr.modify(|_, w| w.pllon().set_bit());
        while rcc.cr.read().pllrdy().bit_is_clear() {}

        // set prescalers and SYSCLK source
        rcc.cfgr.modify(|_, w| {
            w.sw().pll();
            w.hpre().variant(hpre);
            w.ppre1().variant(ppre1);
            w.ppre2().variant(ppre2)
        });

        Clocks {
            sysclk: Hertz(sysclk),
            hclk: Hertz(hclk),
            hpre,
            pclk1: Hertz(pclk1),
            ppre1,
            pclk2: Hertz(pclk2),
            ppre2,
        }
    }

    fn calc_sysclk(&self) -> (u32, u8, PLLSRC_A) {
        let (pllsrc, pllclk) = match self.hse {
            Some(hse) => (PLLSRC_A::HSE_DIV_PREDIV, hse),
            None => (PLLSRC_A::HSI_DIV2, HSI / 2),
        };

        let sysclk = self.sysclk.unwrap_or(pllclk);
        assert!(sysclk % pllclk == 0);

        let pllmul = sysclk / pllclk;
        if let (2..=16) = pllmul {
            let pllmul_bits = pllmul as u8 - 2;
            (sysclk, pllmul_bits, pllsrc)
        } else {
            panic!("invalid PLLMUL");
        }
    }
}

fn set_flash_latency(sysclk: u32, acr: &mut ACR) {
    let latency = if sysclk <= 24_000_000 {
        0b000
    } else if sysclk <= 48_000_000 {
        0b001
    } else {
        0b010
    };

    acr.acr()
        .modify(|_, w| unsafe { w.latency().bits(latency) });
}

fn calc_hpre(sysclk: u32, hclk: u32) -> HPRE_A {
    assert!(sysclk % hclk == 0);

    let hpre = sysclk / hclk;
    match hpre {
        1 => HPRE_A::DIV1,
        2 => HPRE_A::DIV2,
        4 => HPRE_A::DIV4,
        8 => HPRE_A::DIV8,
        16 => HPRE_A::DIV16,
        64 => HPRE_A::DIV64,
        128 => HPRE_A::DIV128,
        256 => HPRE_A::DIV256,
        512 => HPRE_A::DIV512,
        _ => panic!("invalid HPRE: {}", hpre),
    }
}

fn calc_ppre1(hclk: u32, pclk1: u32) -> PPRE1_A {
    assert!(hclk % pclk1 == 0);

    let ppre1 = hclk / pclk1;
    match ppre1 {
        1 => PPRE1_A::DIV1,
        2 => PPRE1_A::DIV2,
        4 => PPRE1_A::DIV4,
        8 => PPRE1_A::DIV8,
        16 => PPRE1_A::DIV16,
        _ => panic!("invalid PPRE1: {}", ppre1),
    }
}

fn calc_ppre2(hclk: u32, pclk2: u32) -> PPRE2_A {
    assert!(hclk % pclk2 == 0);

    let ppre2 = hclk / pclk2;
    match ppre2 {
        1 => PPRE2_A::DIV1,
        2 => PPRE2_A::DIV2,
        4 => PPRE2_A::DIV4,
        8 => PPRE2_A::DIV8,
        16 => PPRE2_A::DIV16,
        _ => panic!("invalid PPRE2: {}", ppre2),
    }
}

/// AMBA High-performance Bus (AHB) registers
pub struct AHB {
    _private: (),
}

impl AHB {
    pub(crate) fn enr(&mut self) -> &pac::rcc::AHBENR {
        // NOTE(unsafe) This proxy grants exclusive access to this register
        unsafe { &(*pac::RCC::ptr()).ahbenr }
    }

    pub(crate) fn rstr(&mut self) -> &pac::rcc::AHBRSTR {
        // NOTE(unsafe) This proxy grants exclusive access to this register
        unsafe { &(*pac::RCC::ptr()).ahbrstr }
    }
}

/// Advanced Peripheral Bus 1 (APB1) registers
pub struct APB1 {
    _private: (),
}

impl APB1 {
    pub(crate) fn enr(&mut self) -> &pac::rcc::APB1ENR {
        // NOTE(unsafe) This proxy grants exclusive access to this register
        unsafe { &(*pac::RCC::ptr()).apb1enr }
    }

    pub(crate) fn rstr(&mut self) -> &pac::rcc::APB1RSTR {
        // NOTE(unsafe) This proxy grants exclusive access to this register
        unsafe { &(*pac::RCC::ptr()).apb1rstr }
    }
}

/// Advanced Peripheral Bus 2 (APB2) registers
pub struct APB2 {
    _private: (),
}

impl APB2 {
    pub(crate) fn enr(&mut self) -> &pac::rcc::APB2ENR {
        // NOTE(unsafe) This proxy grants exclusive access to this register
        unsafe { &(*pac::RCC::ptr()).apb2enr }
    }

    pub(crate) fn rstr(&mut self) -> &pac::rcc::APB2RSTR {
        // NOTE(unsafe) This proxy grants exclusive access to this register
        unsafe { &(*pac::RCC::ptr()).apb2rstr }
    }
}

/// Frozen clock frequencies
///
/// The existence of this value indicates that the clock configuration can no
/// longer be changed.
#[derive(Clone, Copy)]
pub struct Clocks {
    sysclk: Hertz,
    hclk: Hertz,
    hpre: HPRE_A,
    pclk1: Hertz,
    ppre1: PPRE1_A,
    pclk2: Hertz,
    ppre2: PPRE2_A,
}

impl Clocks {
    /// Return the system frequency
    pub fn sysclk(&self) -> Hertz {
        self.sysclk
    }

    /// Return the frequency of the AHB
    pub fn hclk(&self) -> Hertz {
        self.hclk
    }

    fn hpre(&self) -> u16 {
        match self.hpre {
            HPRE_A::DIV1 => 1,
            HPRE_A::DIV2 => 2,
            HPRE_A::DIV4 => 4,
            HPRE_A::DIV8 => 8,
            HPRE_A::DIV16 => 16,
            HPRE_A::DIV64 => 64,
            HPRE_A::DIV128 => 128,
            HPRE_A::DIV256 => 256,
            HPRE_A::DIV512 => 512,
        }
    }

    /// Return the frequency of the APB1
    pub fn pclk1(&self) -> Hertz {
        self.pclk1
    }

    fn ppre1(&self) -> u8 {
        match self.ppre1 {
            PPRE1_A::DIV1 => 1,
            PPRE1_A::DIV2 => 2,
            PPRE1_A::DIV4 => 4,
            PPRE1_A::DIV8 => 8,
            PPRE1_A::DIV16 => 16,
        }
    }

    /// Return the frequency of the APB2
    pub fn pclk2(&self) -> Hertz {
        self.pclk2
    }

    fn ppre2(&self) -> u8 {
        match self.ppre2 {
            PPRE2_A::DIV1 => 1,
            PPRE2_A::DIV2 => 2,
            PPRE2_A::DIV4 => 4,
            PPRE2_A::DIV8 => 8,
            PPRE2_A::DIV16 => 16,
        }
    }
}

/// Trait for system busses
pub trait Bus {
    /// Return the clock frequency of the bus
    fn clock(clocks: Clocks) -> Hertz;
    /// Return the prescaler of the bus
    fn prescaler(clocks: Clocks) -> u16;
}

macro_rules! bus {
    ( $BUS:ty => ($clk:ident, $pre:ident) ) => {
        impl Bus for $BUS {
            fn clock(clocks: Clocks) -> Hertz {
                clocks.$clk()
            }

            fn prescaler(clocks: Clocks) -> u16 {
                u16::from(clocks.$pre())
            }
        }
    };
}

bus!(AHB => (hclk, hpre));
bus!(APB1 => (pclk1, ppre1));
bus!(APB2 => (pclk2, ppre2));

/// Enable and disable a peripheral
pub trait Enable {
    /// Bus the peripheral connects to
    type Bus: Bus;

    /// Enable the peripheral's clock
    fn enable(bus: &mut Self::Bus);
    /// Disable the peripherl's clock
    fn disable(bus: &mut Self::Bus);
}

/// Reset a peripheral
pub trait Reset: Enable {
    /// Reset the peripheral
    fn reset(bus: &mut Self::Bus);
}

macro_rules! clock_control {
    ( $PER:ident => ($BUS:ty, $enr:ident $(, $rstr:ident)*) ) => {
        impl Enable for pac::$PER {
            type Bus = $BUS;

            fn enable(bus: &mut Self::Bus) {
                bus.enr().modify(|_, w| w.$enr().enabled());
            }

            fn disable(bus: &mut Self::Bus) {
                bus.enr().modify(|_, w| w.$enr().disabled());
            }
        }

        $(
            impl Reset for pac::$PER {
                fn reset(bus: &mut Self::Bus) {
                    bus.rstr().modify(|_, w| w.$rstr().reset());
                    bus.rstr().modify(|_, w| w.$rstr().clear_bit());
                }
            }
        )?
    };
}

clock_control!(ADC1_2 => (AHB, adc12en, adc12rst));
clock_control!(ADC3_4 => (AHB, adc34en, adc34rst));

clock_control!(DMA1 => (AHB, dma1en));
clock_control!(DMA2 => (AHB, dma2en));

clock_control!(GPIOA => (AHB, iopaen, ioparst));
clock_control!(GPIOB => (AHB, iopben, iopbrst));
clock_control!(GPIOC => (AHB, iopcen, iopcrst));
clock_control!(GPIOD => (AHB, iopden, iopdrst));
clock_control!(GPIOE => (AHB, iopeen, ioperst));
clock_control!(GPIOF => (AHB, iopfen, iopfrst));
clock_control!(GPIOG => (AHB, iopgen, iopgrst));
clock_control!(GPIOH => (AHB, iophen, iophrst));

clock_control!(SPI1 => (APB2, spi1en, spi1rst));
clock_control!(SPI2 => (APB1, spi2en, spi2rst));
clock_control!(SPI3 => (APB1, spi3en, spi3rst));

clock_control!(TIM1 => (APB2, tim1en, tim1rst));
clock_control!(TIM2 => (APB1, tim2en, tim2rst));
clock_control!(TIM3 => (APB1, tim3en, tim3rst));
clock_control!(TIM4 => (APB1, tim4en, tim4rst));
clock_control!(TIM6 => (APB1, tim6en, tim6rst));
clock_control!(TIM7 => (APB1, tim7en, tim7rst));
clock_control!(TIM8 => (APB2, tim8en, tim8rst));
clock_control!(TIM15 => (APB2, tim15en, tim15rst));
clock_control!(TIM16 => (APB2, tim16en, tim16rst));
clock_control!(TIM17 => (APB2, tim17en, tim17rst));
clock_control!(TIM20 => (APB2, tim20en, tim20rst));
