//! Prelude exporting the extension traits defined in this crate

pub use crate::{dma::DmaExt, flash::FlashExt, gpio::GpioExt, rcc::RccExt, time::U32Ext};
