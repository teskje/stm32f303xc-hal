//! Time units

use crate::rcc::Clocks;
use cortex_m::peripheral::DWT;

/// Hertz
#[derive(Clone, Copy, Debug, PartialEq)]
pub struct Hertz(pub u32);

/// KiloHertz
#[derive(Clone, Copy, Debug, PartialEq)]
pub struct KiloHertz(pub u32);

/// MegaHertz
#[derive(Clone, Copy, Debug, PartialEq)]
pub struct MegaHertz(pub u32);

/// Extension trait that adds convenience methods to the `u32` type
pub trait U32Ext {
    /// Wrap in `Hertz`
    fn hz(self) -> Hertz;
    /// Wrap in `KiloHertz`
    fn khz(self) -> KiloHertz;
    /// Wrap in `MegaHertz`
    fn mhz(self) -> MegaHertz;
}

impl U32Ext for u32 {
    fn hz(self) -> Hertz {
        Hertz(self)
    }

    fn khz(self) -> KiloHertz {
        KiloHertz(self)
    }

    fn mhz(self) -> MegaHertz {
        MegaHertz(self)
    }
}

impl Into<Hertz> for KiloHertz {
    fn into(self) -> Hertz {
        Hertz(self.0 * 1_000)
    }
}

impl Into<Hertz> for MegaHertz {
    fn into(self) -> Hertz {
        Hertz(self.0 * 1_000_000)
    }
}

impl Into<KiloHertz> for MegaHertz {
    fn into(self) -> KiloHertz {
        KiloHertz(self.0 * 1_000)
    }
}

/// A monotonic non-decreasing timer
pub struct MonoTimer {
    freq: Hertz,
}

impl MonoTimer {
    /// Initialize a monotonic timer
    pub fn init(mut dwt: DWT, clocks: Clocks) -> Self {
        dwt.enable_cycle_counter();
        Self {
            freq: clocks.hclk(),
        }
    }

    /// Return the frequency the monotonic timer is operating at
    pub fn frequency(&self) -> Hertz {
        self.freq
    }

    /// Return an `Instant` corresponding to "now"
    pub fn now(&self) -> Instant {
        Instant {
            now: DWT::get_cycle_count(),
        }
    }
}

/// A measurement of a monotonically non-decreasing clock
pub struct Instant {
    now: u32,
}

impl Instant {
    /// Return the ticks elapsed since the `Instant` was created
    pub fn elapsed(&self) -> u32 {
        DWT::get_cycle_count().wrapping_sub(self.now)
    }
}
