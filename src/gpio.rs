//! General-purpose I/Os (GPIO)

use crate::rcc::AHB;
use core::marker::PhantomData;
use cortex_m::interrupt;

/// Extension trait to split a GPIO peripheral into independent pins
pub trait GpioExt {
    /// The type to split the GPIO into
    type Pins;

    /// Split the GPIO block into independent pins
    fn split(self, ahb: &mut AHB) -> Self::Pins;
}

/// Input mode (type state)
pub struct Input<MODE> {
    _mode: PhantomData<MODE>,
}
/// Floating input (type state)
pub struct Floating;
/// Pulled-down input (type state)
pub struct PullDown;
/// Pulled-up input (type state)
pub struct PullUp;

/// Output mode (type state)
pub struct Output<MODE> {
    _mode: PhantomData<MODE>,
}
/// Push-pull output (type state)
pub struct PushPull;
/// Open-drain output (type state)
pub struct OpenDrain;

/// Analog mode (type state)
pub struct Analog;

/// Alternate function 0 (type state)
pub struct AF0;
/// Alternate function 1 (type state)
pub struct AF1;
/// Alternate function 2 (type state)
pub struct AF2;
/// Alternate function 3 (type state)
pub struct AF3;
/// Alternate function 4 (type state)
pub struct AF4;
/// Alternate function 5 (type state)
pub struct AF5;
/// Alternate function 6 (type state)
pub struct AF6;
/// Alternate function 7 (type state)
pub struct AF7;
/// Alternate function 8 (type state)
pub struct AF8;
/// Alternate function 9 (type state)
pub struct AF9;
/// Alternate function 10 (type state)
pub struct AF10;
/// Alternate function 11 (type state)
pub struct AF11;
/// Alternate function 12 (type state)
pub struct AF12;
/// Alternate function 13 (type state)
pub struct AF13;
/// Alternate function 14 (type state)
pub struct AF14;
/// Alternate function 15 (type state)
pub struct AF15;

macro_rules! gpio {
    (
        port: ($GPIOX:ident, $gpiox:ident, $gpioy:ident),
        pins: [$(
            ($PXi:ident, $pxi:ident, $mode:ty, $afr:ident, $afri:ident,
             $moderi:ident, $oti:ident, $pupdri:ident, $bsi:ident, $bri:ident,
             [$( ($AFx:ty, $afx:ident, $into_afx:ident) ),*]),
        )+],
    ) => {
        /// GPIOx
        pub mod $gpiox {
            use super::*;
            use crate::{
                pac::{self, $GPIOX},
                rcc::{Enable, Reset},
            };

            impl GpioExt for $GPIOX {
                type Pins = Pins;

                fn split(self, ahb: &mut AHB) -> Pins {
                    $GPIOX::enable(ahb);
                    $GPIOX::reset(ahb);

                    Pins {
                        $( $pxi: $PXi { _mode: PhantomData }, )+
                    }
                }
            }

            unsafe fn moder() -> &'static pac::$gpioy::MODER {
                &(*pac::$GPIOX::ptr()).moder
            }

            unsafe fn pupdr() -> &'static pac::$gpioy::PUPDR {
                &(*pac::$GPIOX::ptr()).pupdr
            }

            unsafe fn otyper() -> &'static pac::$gpioy::OTYPER {
                &(*pac::$GPIOX::ptr()).otyper
            }

            unsafe fn bsrr() -> &'static pac::$gpioy::BSRR {
                &(*pac::$GPIOX::ptr()).bsrr
            }

            /// GPIOx pins
            pub struct Pins {
                $(
                    /// Pin
                    pub $pxi: $PXi<$mode>,
                )+
            }

            $(
                /// Pin
                pub struct $PXi<MODE> {
                    _mode: PhantomData<MODE>,
                }

                impl<MODE> $PXi<MODE> {
                    /// Configure the pin to operate as a floating input pin
                    pub fn into_floating_input(self) -> $PXi<Input<Floating>> {
                        // NOTE(unsafe) critical section prevents races
                        interrupt::free(|_| unsafe {
                            moder().modify(|_, w| w.$moderi().input());
                            pupdr().modify(|_, w| w.$pupdri().floating());
                        });
                        $PXi { _mode: PhantomData }
                    }

                    /// Configure the pin to operate as a pull-down input pin
                    pub fn into_pull_down_input(self) -> $PXi<Input<PullDown>> {
                        // NOTE(unsafe) critical section prevents races
                        interrupt::free(|_| unsafe {
                            moder().modify(|_, w| w.$moderi().input());
                            pupdr().modify(|_, w| w.$pupdri().pull_down());
                        });
                        $PXi { _mode: PhantomData }
                    }

                    /// Configure the pin to operate as a pull-up input pin
                    pub fn into_pull_up_input(self) -> $PXi<Input<PullUp>> {
                        // NOTE(unsafe) critical section prevents races
                        interrupt::free(|_| unsafe {
                            moder().modify(|_, w| w.$moderi().input());
                            pupdr().modify(|_, w| w.$pupdri().pull_up());
                        });
                        $PXi { _mode: PhantomData }
                    }

                    /// Configure the pin to operate as a open-drain output pin
                    pub fn into_open_drain_output(self) -> $PXi<Output<OpenDrain>> {
                        // NOTE(unsafe) critical section prevents races
                        interrupt::free(|_| unsafe {
                            moder().modify(|_, w| w.$moderi().output());
                            otyper().modify(|_, w| w.$oti().open_drain());
                        });
                        $PXi { _mode: PhantomData }
                    }

                    /// Configure the pin to operate as a push-pull output pin
                    pub fn into_push_pull_output(self) -> $PXi<Output<PushPull>> {
                        // NOTE(unsafe) critical section prevents races
                        interrupt::free(|_| unsafe {
                            moder().modify(|_, w| w.$moderi().output());
                            otyper().modify(|_, w| w.$oti().push_pull());
                        });
                        $PXi { _mode: PhantomData }
                    }

                    /// Configure the pin to operate as an analog pin
                    pub fn into_analog(self) -> $PXi<Analog> {
                        // NOTE(unsafe) critical section prevents races
                        interrupt::free(|_| unsafe {
                            moder().modify(|_, w| w.$moderi().analog());
                            pupdr().modify(|_, w| w.$pupdri().floating());
                        });
                        $PXi { _mode: PhantomData }
                    }

                    $(
                        /// Configure the pin to operate as an alternate function
                        pub fn $into_afx(self) -> $PXi<$AFx> {
                            // NOTE(unsafe) critical section prevents races
                            interrupt::free(|_| unsafe {
                                moder().modify(|_, w| w.$moderi().alternate());
                                let afr = &(*pac::$GPIOX::ptr()).$afr;
                                afr.modify(|_, w| w.$afri().$afx());
                            });
                            $PXi { _mode: PhantomData }
                        }
                    )*
                }

                impl<O> $PXi<Output<O>> {
                    /// Set this pin's output to high
                    pub fn set_high(&mut self) {
                        // NOTE(unsafe) atomic write to a stateless register
                        unsafe {
                            bsrr().write(|w| w.$bsi().set_bit());
                        }
                    }

                    /// Set this pin's output to low
                    pub fn set_low(&mut self) {
                        // NOTE(unsafe) atomic write to a stateless register
                        unsafe {
                            bsrr().write(|w| w.$bri().set_bit());
                        }
                    }
                }
            )+
        }
    };

    (
        port: ($X:ident/$x:ident, pac: $y:ident),
        pins: [ $( $i:expr => ($mode:ty, $afr:ident: [$( $af:expr ),*]), )+ ],
    ) => {
        paste::item! {
            gpio! {
                port: ([<GPIO $X>], [<gpio $x>], [<gpio $y>]),
                pins: [$(
                    ([<P $X $i>], [<p $x $i>], $mode, $afr, [<$afr $i>],
                     [<moder $i>], [<ot $i>], [<pupdr $i>], [<bs $i>], [<br $i>],
                     [$( ([<AF $af>], [<af $af>], [<into_af $af>]) ),*]),
                )+],
            }
        }
    };
}

gpio! {
    port: (A/a, pac: a),
    pins: [
        0 => (Input<Floating>, afrl: [1, 3, 7, 8, 9, 10, 15]),
        1 => (Input<Floating>, afrl: [0, 1, 3, 7, 9, 15]),
        2 => (Input<Floating>, afrl: [1, 3, 7, 8, 9, 15]),
        3 => (Input<Floating>, afrl: [1, 3, 7, 9, 15]),
        4 => (Input<Floating>, afrl: [2, 3, 5, 6, 7, 15]),
        5 => (Input<Floating>, afrl: [1, 3, 5, 15]),
        6 => (Input<Floating>, afrl: [1, 2, 3, 4, 5, 6, 8, 15]),
        7 => (Input<Floating>, afrl: [1, 2, 3, 4, 5, 6, 8, 15]),
        8 => (Input<Floating>, afrh: [0, 4, 5, 6, 7, 8, 10, 15]),
        9 => (Input<Floating>, afrh: [3, 4, 5, 6, 7, 8, 9, 10, 15]),
        10 => (Input<Floating>, afrh: [1, 3, 4, 6, 7, 8, 10, 11, 15]),
        11 => (Input<Floating>, afrh: [6, 7, 8, 9, 10, 11, 12, 14, 15]),
        12 => (Input<Floating>, afrh: [1, 6, 7, 8, 9, 10, 11, 14, 15]),
        13 => (AF0, afrh: [0, 1, 3, 5, 7, 10, 15]),
        14 => (AF0, afrh: [0, 3, 4, 5, 6, 7, 15]),
        15 => (AF0, afrh: [0, 1, 2, 4, 5, 6, 7, 9, 15]),
    ],
}

gpio! {
    port: (B/b, pac: b),
    pins: [
        0 => (Input<Floating>, afrl: [2, 3, 4, 6, 15]),
        1 => (Input<Floating>, afrl: [2, 3, 4, 6, 8, 15]),
        2 => (Input<Floating>, afrl: [3, 15]),
        3 => (AF0, afrl: [0, 1, 2, 3, 4, 5, 6, 7, 10, 15]),
        4 => (AF0, afrl: [0, 1, 2, 3, 4, 5, 6, 7, 10, 15]),
        5 => (Input<Floating>, afrl: [1, 2, 3, 4, 5, 6, 7, 10, 15]),
        6 => (Input<Floating>, afrl: [1, 2, 3, 4, 5, 6, 7, 10, 15]),
        7 => (Input<Floating>, afrl: [1, 2, 3, 4, 5, 7, 10, 15]),
        8 => (Input<Floating>, afrh: [1, 2, 3, 4, 8, 9, 10, 12, 15]),
        9 => (Input<Floating>, afrh: [1, 2, 4, 6, 8, 9, 10, 15]),
        10 => (Input<Floating>, afrh: [1, 3, 7, 15]),
        11 => (Input<Floating>, afrh: [1, 3, 7, 15]),
        12 => (Input<Floating>, afrh: [3, 4, 5, 6, 7, 15]),
        13 => (Input<Floating>, afrh: [3, 5, 6, 7, 15]),
        14 => (Input<Floating>, afrh: [1, 3, 5, 6, 7, 15]),
        15 => (Input<Floating>, afrh: [0, 1, 2, 4, 5, 15]),
    ],
}

gpio! {
    port: (C/c, pac: b),
    pins: [
        0 => (Input<Floating>, afrl: [1]),
        1 => (Input<Floating>, afrl: [1]),
        2 => (Input<Floating>, afrl: [1, 3]),
        3 => (Input<Floating>, afrl: [1, 6]),
        4 => (Input<Floating>, afrl: [1, 7]),
        5 => (Input<Floating>, afrl: [1, 3, 7]),
        6 => (Input<Floating>, afrl: [1, 2, 4, 6, 7]),
        7 => (Input<Floating>, afrl: [1, 2, 4, 6, 7]),
        8 => (Input<Floating>, afrh: [1, 2, 4, 7]),
        9 => (Input<Floating>, afrh: [1, 2, 4, 5, 6]),
        10 => (Input<Floating>, afrh: [1, 4, 5, 6, 7]),
        11 => (Input<Floating>, afrh: [1, 4, 5, 6, 7]),
        12 => (Input<Floating>, afrh: [1, 4, 5, 6, 7]),
        13 => (Input<Floating>, afrh: [4]),
        14 => (Input<Floating>, afrh: []),
        15 => (Input<Floating>, afrh: []),
    ],
}

gpio! {
    port: (D/d, pac: b),
    pins: [
        0 => (Input<Floating>, afrl: [1, 7]),
        1 => (Input<Floating>, afrl: [1, 4, 6, 7]),
        2 => (Input<Floating>, afrl: [1, 2, 4, 5]),
        3 => (Input<Floating>, afrl: [1, 2, 7]),
        4 => (Input<Floating>, afrl: [1, 2, 7]),
        5 => (Input<Floating>, afrl: [1, 7]),
        6 => (Input<Floating>, afrl: [1, 2, 7]),
        7 => (Input<Floating>, afrl: [1, 2, 7]),
        8 => (Input<Floating>, afrh: [1, 7]),
        9 => (Input<Floating>, afrh: [1, 7]),
        10 => (Input<Floating>, afrh: [1, 7]),
        11 => (Input<Floating>, afrh: [1, 7]),
        12 => (Input<Floating>, afrh: [1, 2, 3, 7]),
        13 => (Input<Floating>, afrh: [1, 2, 3]),
        14 => (Input<Floating>, afrh: [1, 2, 3]),
        15 => (Input<Floating>, afrh: [1, 2, 3, 6]),
    ],
}

gpio! {
    port: (E/e, pac: b),
    pins: [
        0 => (Input<Floating>, afrl: [1, 2, 4, 7]),
        1 => (Input<Floating>, afrl: [1, 4, 7]),
        2 => (Input<Floating>, afrl: [0, 1, 2, 3]),
        3 => (Input<Floating>, afrl: [0, 1, 2, 3]),
        4 => (Input<Floating>, afrl: [0, 1, 2, 3]),
        5 => (Input<Floating>, afrl: [0, 1, 2, 3]),
        6 => (Input<Floating>, afrl: [0, 1]),
        7 => (Input<Floating>, afrl: [1, 2]),
        8 => (Input<Floating>, afrh: [1, 2]),
        9 => (Input<Floating>, afrh: [1, 2]),
        10 => (Input<Floating>, afrh: [1, 2]),
        11 => (Input<Floating>, afrh: [1, 2]),
        12 => (Input<Floating>, afrh: [1, 2]),
        13 => (Input<Floating>, afrh: [1, 2]),
        14 => (Input<Floating>, afrh: [1, 2, 6]),
        15 => (Input<Floating>, afrh: [1, 2, 7]),
    ],
}

gpio! {
    port: (F/f, pac: b),
    pins: [
        0 => (Input<Floating>, afrl: [4, 6]),
        1 => (Input<Floating>, afrl: [4]),
        2 => (Input<Floating>, afrl: [1]),
        3 => (Input<Floating>, afrl: []),
        4 => (Input<Floating>, afrl: [1, 2]),
        5 => (Input<Floating>, afrl: []),
        6 => (Input<Floating>, afrl: [1, 2, 4, 7]),
        7 => (Input<Floating>, afrl: []),
        8 => (Input<Floating>, afrh: []),
        9 => (Input<Floating>, afrh: [1, 3, 5]),
        10 => (Input<Floating>, afrh: [1, 3, 5]),
        11 => (Input<Floating>, afrh: []),
        12 => (Input<Floating>, afrh: []),
        13 => (Input<Floating>, afrh: []),
        14 => (Input<Floating>, afrh: []),
        15 => (Input<Floating>, afrh: []),
    ],
}

gpio! {
    port: (G/g, pac: b),
    pins: [
        0 => (Input<Floating>, afrl: []),
        1 => (Input<Floating>, afrl: []),
        2 => (Input<Floating>, afrl: []),
        3 => (Input<Floating>, afrl: []),
        4 => (Input<Floating>, afrl: []),
        5 => (Input<Floating>, afrl: []),
        6 => (Input<Floating>, afrl: []),
        7 => (Input<Floating>, afrl: []),
        8 => (Input<Floating>, afrh: []),
        9 => (Input<Floating>, afrh: []),
        10 => (Input<Floating>, afrh: []),
        11 => (Input<Floating>, afrh: []),
        12 => (Input<Floating>, afrh: []),
        13 => (Input<Floating>, afrh: []),
        14 => (Input<Floating>, afrh: []),
        15 => (Input<Floating>, afrh: []),
    ],
}

gpio! {
    port: (H/h, pac: b),
    pins: [
        0 => (Input<Floating>, afrl: []),
        1 => (Input<Floating>, afrl: []),
        2 => (Input<Floating>, afrl: []),
        3 => (Input<Floating>, afrl: []),
        4 => (Input<Floating>, afrl: []),
        5 => (Input<Floating>, afrl: []),
        6 => (Input<Floating>, afrl: []),
        7 => (Input<Floating>, afrl: []),
        8 => (Input<Floating>, afrh: []),
        9 => (Input<Floating>, afrh: []),
        10 => (Input<Floating>, afrh: []),
        11 => (Input<Floating>, afrh: []),
        12 => (Input<Floating>, afrh: []),
        13 => (Input<Floating>, afrh: []),
        14 => (Input<Floating>, afrh: []),
        15 => (Input<Floating>, afrh: []),
    ],
}
