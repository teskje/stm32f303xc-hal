//! Timers

use crate::{
    pac,
    rcc::{Bus, Clocks, Enable, Reset},
    time::Hertz,
};
use cast::From as _;

/// Periodic count-down timer
pub struct Timer<TIM> {
    tim: TIM,
    clocks: Clocks,
}

/// Interrupt events
pub enum Event {
    /// Counter over-/underflow
    Update,
}

/// Master mode trigger output
pub enum Trgo {
    /// UG bit
    Reset,
    /// Counter enable signal
    Enable,
    /// Update event
    Update,
}

macro_rules! timer {
    ( $TIMx:ident, $timx:ident, $arr_ty:ty ) => {
        use pac::$TIMx;

        impl Timer<$TIMx> {
            /// Configure a `TIM` peripheral as a periodic count-down timer
            pub fn $timx(tim: $TIMx, clocks: Clocks, apb: &mut <$TIMx as Enable>::Bus) -> Self {
                $TIMx::enable(apb);
                $TIMx::reset(apb);

                Self { tim, clocks }
            }

            fn set_frequency(&mut self, freq: Hertz) {
                self.stop();

                let ticks = self.clock().0 / freq.0;

                let psc = u16::cast(ticks - 1).unwrap();
                let arr = (ticks / (u32::from(psc) + 1)) as $arr_ty;
                self.tim.psc.write(|w| w.psc().bits(psc));

                // FIXME(pac) writing to ARR is unsafe for some TIMx
                #[allow(unused_unsafe)]
                self.tim.arr.write(|w| unsafe { w.arr().bits(arr) });

                // trigger update event to load the prescaler value to the clock
                self.tim.egr.write(|w| w.ug().update());
                self.clear_interrupt(Event::Update);
            }

            fn clock(&self) -> Hertz {
                let bus_clock = <$TIMx as Enable>::Bus::clock(self.clocks);
                let ppre = <$TIMx as Enable>::Bus::prescaler(self.clocks);

                let mul = if ppre == 1 { 1 } else { 2 };
                Hertz(bus_clock.0 * mul)
            }

            /// Start the timer
            pub fn start<F>(&mut self, freq: F)
            where
                F: Into<Hertz>,
            {
                self.set_frequency(freq.into());
                self.tim.cr1.modify(|_, w| w.cen().enabled());
            }

            /// Stop the timer
            pub fn stop(&mut self) {
                self.tim.cr1.modify(|_, w| w.cen().disabled());
            }

            /// Start listening for an event
            pub fn listen(&mut self, event: Event) {
                match event {
                    Event::Update => self.tim.dier.write(|w| w.uie().enabled()),
                }
            }

            /// Stop listening for an event
            pub fn unlisten(&mut self, event: Event) {
                match event {
                    Event::Update => self.tim.dier.write(|w| w.uie().disabled()),
                }
            }

            /// Clear the update interrupt flag
            pub fn clear_interrupt(&mut self, event: Event) {
                match event {
                    Event::Update => self.tim.sr.modify(|_, w| w.uif().clear()),
                }
            }

            /// Select the timer's master mode
            pub fn set_master_mode(&mut self, trgo: Trgo) {
                match trgo {
                    Trgo::Reset => self.tim.cr2.modify(|_, w| w.mms().reset()),
                    Trgo::Enable => self.tim.cr2.modify(|_, w| w.mms().enable()),
                    Trgo::Update => self.tim.cr2.modify(|_, w| w.mms().update()),
                }
            }
        }
    };

    ( $x:expr => { arr: $arr_ty:ty } ) => {
        paste::item! {
            timer!([<TIM $x>], [<tim $x>], $arr_ty);
        }
    };
}

timer!(1 => { arr: u16 });
timer!(2 => { arr: u32 });
timer!(3 => { arr: u16 });
timer!(4 => { arr: u16 });
timer!(6 => { arr: u16 });
timer!(7 => { arr: u16 });
timer!(8 => { arr: u16 });
timer!(20 => { arr: u16 });

// FIXME(pac) These timers are missing methods on MMS_W
// timer!(15 => { arr: u16 });
// timer!(16 => { arr: u16 });
// timer!(17 => { arr: u16 });
