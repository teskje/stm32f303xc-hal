//! Analog-to-digital converters

use crate::{
    dma::{self, dma1, dma2},
    gpio::{gpioa, gpiob, gpioc, gpiod, gpioe, gpiof, Analog},
    pac::{self, adc1::cfgr::EXTSEL_A, ADC1, ADC1_2, ADC2, ADC3, ADC3_4, ADC4},
    rcc::{Clocks, Enable, Reset},
    timer::Timer,
};
use as_slice::AsMutSlice;
use core::ops::Deref;
use core::{
    marker::PhantomData,
    sync::atomic::{self, Ordering},
};
use cortex_m::asm;

/// ADC disabled: ADEN=0 (type state)
pub struct Disabled;
/// ADC stopped: ADEN=1, ADSTART=0 (type state)
pub struct Stopped;
/// ADC started: ADEN=1, ADSTART=1 (type state)
pub struct Started;

/// A configured ADC
pub struct Adc<ADC, STATE> {
    adc: ADC,
    clocks: Clocks,
    _state: PhantomData<STATE>,
}

impl<ADC> Adc<ADC, Disabled>
where
    ADC: Deref<Target = pac::adc1::RegisterBlock>,
{
    fn enable_adcvreg(&mut self) {
        self.adc.cr.modify(|_, w| w.advregen().intermediate());
        self.adc.cr.modify(|_, w| w.advregen().enabled());

        // delay for the worst-case voltage regulator startup time
        delay_us(10, self.clocks);
    }

    fn calibrate(&mut self) {
        self.adc.cr.modify(|_, w| w.adcal().calibration());
        while self.adc.cr.read().adcal().is_calibration() {}

        // ADEN bit cannot be set during 4 ADC clock cycles after end
        // of calibration. So delay a bit here, just to be sure.
        delay_us(1, self.clocks);
    }

    /// Enable the ADC
    pub fn enable(self) -> Adc<ADC, Stopped> {
        self.adc.cr.modify(|_, w| w.aden().enable());
        while self.adc.isr.read().adrdy().is_not_ready() {}

        Adc {
            adc: self.adc,
            clocks: self.clocks,
            _state: PhantomData,
        }
    }
}

impl<ADC> Adc<ADC, Stopped>
where
    ADC: Deref<Target = pac::adc1::RegisterBlock>,
{
    /// Set the channel for ADC conversion
    pub fn set_channel<P>(&mut self, _: P)
    where
        P: Channel<ADC>,
    {
        let channel = P::channel();
        self.adc
            .sqr1
            .modify(|_, w| unsafe { w.sq1().bits(channel) });
    }

    /// Set external trigger for ADC conversion
    pub fn set_external_trigger<T>(&mut self, _: &T)
    where
        T: ExternalTrigger<ADC>,
    {
        let extsel = T::extsel();
        self.adc.cfgr.modify(|_, w| {
            w.exten().rising_edge();
            w.extsel().variant(extsel)
        });
    }

    /// Enable end-of-conversion interrupt
    pub fn enable_eoc_interrupt(self) {
        self.adc.ier.modify(|_, w| w.eocie().enabled());
    }

    /// Start analog-to-digital conversion
    pub fn start(self) -> Adc<ADC, Started> {
        self.adc.cr.modify(|_, w| w.adstart().start());

        Adc {
            adc: self.adc,
            clocks: self.clocks,
            _state: PhantomData,
        }
    }
}

impl<ADC> Adc<ADC, Started>
where
    ADC: Deref<Target = pac::adc1::RegisterBlock>,
{
    /// Stop analog-to-digital conversion
    pub fn stop(self) -> Adc<ADC, Stopped> {
        self.adc.cr.modify(|_, w| w.adstart().clear_bit());

        Adc {
            adc: self.adc,
            clocks: self.clocks,
            _state: PhantomData,
        }
    }
}

impl<ADC, S> Adc<ADC, S>
where
    ADC: Deref<Target = pac::adc1::RegisterBlock>,
{
    /// Read the result from the last conversion
    pub fn read(&self) -> u16 {
        self.adc.dr.read().rdata().bits()
    }
}

macro_rules! adc {
    ( $ADCx:ty, $adcx:ident, $ADCx_y:ty ) => {
        impl Adc<$ADCx, Disabled> {
            /// Initialize an ADC
            pub fn $adcx(
                adc: $ADCx,
                adc_shared: &mut $ADCx_y,
                clocks: Clocks,
                ahb: &mut <$ADCx_y as Enable>::Bus,
            ) -> Self {
                <$ADCx_y>::enable(ahb);
                <$ADCx_y>::reset(ahb);

                // synchronous clock mode: HCLK/2
                adc_shared
                    .ccr
                    .modify(|_, w| unsafe { w.ckmode().bits(0b10) });

                let mut self_ = Self {
                    adc,
                    clocks,
                    _state: PhantomData,
                };

                self_.enable_adcvreg();
                self_.calibrate();
                self_
            }
        }
    };
}

adc!(ADC1, adc1, ADC1_2);
adc!(ADC2, adc2, ADC1_2);
adc!(ADC3, adc3, ADC3_4);
adc!(ADC4, adc4, ADC3_4);

fn delay_us(us: u32, clocks: Clocks) {
    let cycles = us * (clocks.hclk().0 / 1_000_000);
    asm::delay(cycles);
}

/// A channel that can be used with an ADC
pub trait Channel<ADC> {
    /// Return the channel ID
    fn channel() -> u8;
}

macro_rules! adc_pins {
    ( $ADCx:ident, $( $pin:ty => $chan:expr, )+ ) => {
        $(
            impl Channel<pac::$ADCx> for $pin {
                fn channel() -> u8 {
                    $chan
                }
            }
        )+
    };
}

adc_pins!(ADC1,
    gpioa::PA0<Analog> => 1,
    gpioa::PA1<Analog> => 2,
    gpioa::PA2<Analog> => 3,
    gpioa::PA3<Analog> => 4,
    gpiof::PF4<Analog> => 5,
    gpioc::PC0<Analog> => 6,
    gpioc::PC1<Analog> => 7,
    gpioc::PC2<Analog> => 8,
    gpioc::PC3<Analog> => 9,
    gpiof::PF2<Analog> => 10,
);

adc_pins!(ADC2,
    gpioa::PA4<Analog> => 1,
    gpioa::PA5<Analog> => 2,
    gpioa::PA6<Analog> => 3,
    gpioa::PA7<Analog> => 4,
    gpioc::PC4<Analog> => 5,
    gpioc::PC0<Analog> => 6,
    gpioc::PC1<Analog> => 7,
    gpioc::PC2<Analog> => 8,
    gpioc::PC3<Analog> => 9,
    gpiof::PF2<Analog> => 10,
    gpioc::PC5<Analog> => 11,
    gpiob::PB2<Analog> => 12,
);

adc_pins!(ADC3,
    gpiob::PB1<Analog> => 1,
    gpioe::PE9<Analog> => 2,
    gpioe::PE13<Analog> => 3,
    gpiob::PB13<Analog> => 5,
    gpioe::PE8<Analog> => 6,
    gpiod::PD10<Analog> => 7,
    gpiod::PD11<Analog> => 8,
    gpiod::PD12<Analog> => 9,
    gpiod::PD13<Analog> => 10,
    gpiod::PD14<Analog> => 11,
    gpiob::PB0<Analog> => 12,
    gpioe::PE7<Analog> => 13,
    gpioe::PE10<Analog> => 14,
    gpioe::PE11<Analog> => 15,
    gpioe::PE12<Analog> => 16,
);

adc_pins!(ADC4,
    gpioe::PE14<Analog> => 1,
    gpioe::PE15<Analog> => 2,
    gpiob::PB12<Analog> => 3,
    gpiob::PB14<Analog> => 4,
    gpiob::PB15<Analog> => 5,
    gpioe::PE8<Analog> => 6,
    gpiod::PD10<Analog> => 7,
    gpiod::PD11<Analog> => 8,
    gpiod::PD12<Analog> => 9,
    gpiod::PD13<Analog> => 10,
    gpiod::PD14<Analog> => 11,
    gpiod::PD8<Analog> => 12,
    gpiod::PD9<Analog> => 13,
);

/// A peripheral that can trigger ADC conversions
pub trait ExternalTrigger<ADC> {
    /// `EXTSEL` value for this trigger
    fn extsel() -> EXTSEL_A;
}

macro_rules! adc_triggers {
    ( $ADCx:ident, $( $T:ty => $extsel:expr, )+ ) => {
        $(
            impl ExternalTrigger<pac::$ADCx> for $T {
                fn extsel() -> EXTSEL_A {
                    $extsel
                }
            }
        )+
    };
}

adc_triggers!(ADC1,
    Timer<pac::TIM1> => EXTSEL_A::TIM1_TRGO,
    Timer<pac::TIM2> => EXTSEL_A::TIM2_TRGO,
    Timer<pac::TIM3> => EXTSEL_A::TIM3_TRGO,
    Timer<pac::TIM6> => EXTSEL_A::TIM6_TRGO,
    Timer<pac::TIM15> => EXTSEL_A::TIM15_TRGO,
);

adc_triggers!(ADC2,
    Timer<pac::TIM1> => EXTSEL_A::TIM1_TRGO,
    Timer<pac::TIM2> => EXTSEL_A::TIM2_TRGO,
    Timer<pac::TIM3> => EXTSEL_A::TIM3_TRGO,
    Timer<pac::TIM6> => EXTSEL_A::TIM6_TRGO,
    Timer<pac::TIM15> => EXTSEL_A::TIM15_TRGO,
);

/// DMA on an ADC
pub struct AdcDma<ADC, C> {
    adc: Adc<ADC, Stopped>,
    channel: C,
}

macro_rules! adc_dma {
    ( $ADCx:ident => $chan:ty ) => {
        impl Adc<pac::$ADCx, Stopped> {
            /// Enable DMA mode for this ADC
            pub fn into_dma(self, channel: $chan) -> AdcDma<pac::$ADCx, $chan> {
                self.adc.cfgr.modify(|_, w| w.dmaen().enabled());
                AdcDma { adc: self, channel }
            }
        }

        impl AdcDma<pac::$ADCx, $chan> {
            /// Start reading from the ADC via DMA
            pub fn start<B>(
                mut self,
                buf: &'static mut B,
            ) -> dma::Transfer<B, $chan, Adc<pac::$ADCx, Started>>
            where
                B: AsMutSlice<Element = u16>,
            {
                let slice = buf.as_mut_slice();
                self.configure_channel(slice);

                atomic::compiler_fence(Ordering::SeqCst);

                self.channel.start_read();
                let payload = self.adc.start();

                dma::Transfer::new(buf, self.channel, payload)
            }

            /// Start reading from the ADC via DMA in circular mode
            pub fn start_circular<B>(
                mut self,
                buf: &'static mut [B; 2],
            ) -> dma::CircularTransfer<B, $chan, Adc<pac::$ADCx, Started>>
            where
                B: AsMutSlice<Element = u16>,
            {
                let slice = buf[0].as_mut_slice();
                self.configure_channel(slice);
                self.channel.set_transfer_length(slice.len() * 2);

                self.channel.enable_circular_mode();
                self.adc.adc.cfgr.modify(|_, w| w.dmacfg().circular());

                atomic::compiler_fence(Ordering::SeqCst);

                self.channel.start_read();
                let payload = self.adc.start();

                dma::CircularTransfer::new(buf, self.channel, payload)
            }

            fn configure_channel(&mut self, slice: &mut [u16]) {
                use dma::{Increment, WordSize};

                let pa = &self.adc.adc.dr as *const _ as u32;
                let ma = slice.as_ptr() as u32;
                self.channel.set_peripheral_address(pa, Increment::Disable);
                self.channel.set_memory_address(ma, Increment::Enable);

                self.channel.set_transfer_length(slice.len());
                self.channel.set_word_size(WordSize::Bits16);
            }
        }
    };
}

adc_dma!(ADC1 => dma1::C1);
adc_dma!(ADC2 => dma2::C1);
adc_dma!(ADC3 => dma2::C5);
adc_dma!(ADC4 => dma2::C2);
