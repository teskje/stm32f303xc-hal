//! Direct memory access controller (DMA)

use crate::rcc::AHB;
use cast::From as _;
use core::sync::atomic::{self, Ordering};

/// Extension trait to split a DMA peripheral into independent channels
pub trait DmaExt {
    /// The type to split the DMA into
    type Channels;

    /// Split the DMA into independent channels
    fn split(self, ahb: &mut AHB) -> Self::Channels;
}

/// An in-progress DMA transfer
///
/// By requiring a `&'static mut` buffer, we ensure statically that:
///  - The buffer is not freed while a transfer is in progress.
///  - The transfer has exclusive access to the buffer.
pub struct Transfer<B, C, P>
where
    B: 'static,
{
    buf: &'static mut B,
    channel: C,
    payload: P,
}

impl<B, C, P> Transfer<B, C, P> {
    pub(crate) fn new(buf: &'static mut B, channel: C, payload: P) -> Self {
        Self {
            buf,
            channel,
            payload,
        }
    }
}

/// An in-progress circular DMA transfer
///
/// In contrast to the regular `Transfer`, one half of the buffer is
/// always readable. This makes it possible to access the transferred
/// data without stopping the transfer.
pub struct CircularTransfer<B, C, P>
where
    B: 'static,
{
    buf: &'static mut [B; 2],
    channel: C,
    _payload: P,
    readable_half: Half,
}

impl<B, C, P> CircularTransfer<B, C, P> {
    pub(crate) fn new(buf: &'static mut [B; 2], channel: C, payload: P) -> Self {
        Self {
            buf,
            channel,
            _payload: payload,
            readable_half: Half::Second,
        }
    }
}

#[derive(Debug)]
enum Half {
    First,
    Second,
}

/// Error returned from DMA operations
#[derive(Debug)]
pub enum Error {
    /// DMA buffer was overwritten while being read
    Overrun,
}

/// DMA address increment mode
#[derive(Debug)]
pub enum Increment {
    /// Increment mode enabled
    Enable,
    /// Increment mode disabled
    Disable,
}

/// DMA word size
#[derive(Debug)]
pub enum WordSize {
    /// 8 bits
    Bits8,
    /// 16 bits
    Bits16,
    /// 32 bits
    Bits32,
}

/// Channel priority level
#[derive(Debug)]
pub enum Priority {
    /// Low
    Low,
    /// Medium
    Medium,
    /// High
    High,
    /// Very high
    VeryHigh,
}

/// DMA event
#[derive(Debug)]
pub enum Event {
    /// First half of a transfer is done
    HalfTransfer,
    /// Transfer is complete
    TransferComplete,
}

/// Payload of a (non-circular) DMA transfer
pub trait TransferPayload {
    /// Stop DMA on the payload
    fn stop(&mut self);
}

macro_rules! dma {
    (
        controller: ($DMAx:ident, $dmax:ident),
        channels: [
            $( ($Ci:ident, $chi:ident,
                $tcifi:ident, $htifi:ident, $ctcifi:ident, $chtifi:ident), )+
        ],
    ) => {
        /// DMAx
        pub mod $dmax {
            use super::*;
            use crate::{
                pac::{self, $DMAx},
                rcc::Enable,
            };

            impl DmaExt for $DMAx {
                type Channels = Channels;

                fn split(self, ahb: &mut AHB) -> Channels {
                    $DMAx::enable(ahb);

                    // reset channel control registers to stop ongoing transfers
                    $(
                        self.$chi.cr.reset();
                    )+

                    Channels {
                        $( $chi: $Ci { _private: () }, )+
                    }
                }
            }

            fn read_isr() -> pac::dma1::isr::R {
                // NOTE(unsafe) atomic read
                unsafe { (*$DMAx::ptr()).isr.read() }
            }

            unsafe fn ifcr() -> &'static pac::dma1::IFCR {
                &(*$DMAx::ptr()).ifcr
            }

            /// DMAx channels
            pub struct Channels {
                $(
                    /// Channel
                    pub $chi: $Ci,
                )+
            }

            $(
                /// Singleton that represents a DMAx channel
                pub struct $Ci {
                    _private: (),
                }

                impl $Ci {
                    fn ch(&mut self) -> &pac::dma1::CH {
                        // NOTE(unsafe) $Ci grants exclusive access to this register
                        unsafe { &(*$DMAx::ptr()).$chi }
                    }

                    /// Set the base address of the peripheral data register
                    /// from/to which the data will be read/written
                    pub fn set_peripheral_address(&mut self, address: u32, inc: Increment) {
                        self.ch().par.write(|w| w.pa().bits(address));
                        match inc {
                            Increment::Enable => self.ch().cr.modify(|_, w| w.pinc().enabled()),
                            Increment::Disable => self.ch().cr.modify(|_, w| w.pinc().disabled()),
                        }
                    }

                    /// Set the base address of the memory area from/to which
                    /// the data will be read/written
                    pub fn set_memory_address(&mut self, address: u32, inc: Increment) {
                        self.ch().mar.write(|w| w.ma().bits(address));
                        match inc {
                            Increment::Enable => self.ch().cr.modify(|_, w| w.minc().enabled()),
                            Increment::Disable => self.ch().cr.modify(|_, w| w.minc().disabled()),
                        }
                    }

                    /// Set the number of words to transfer
                    pub fn set_transfer_length(&mut self, len: usize) {
                        let len = u16::cast(len).expect("DMA transfer too large");
                        self.ch().ndtr.write(|w| w.ndt().bits(len));
                    }

                    /// Set the word size
                    pub fn set_word_size(&mut self, size: WordSize) {
                        use pac::dma1::ch::cr::{MSIZE_A, PSIZE_A};
                        let (psize, msize) = match size {
                            WordSize::Bits8 => (PSIZE_A::BITS8, MSIZE_A::BITS8),
                            WordSize::Bits16 => (PSIZE_A::BITS16, MSIZE_A::BITS16),
                            WordSize::Bits32 => (PSIZE_A::BITS32, MSIZE_A::BITS32),
                        };
                        self.ch().cr.modify(|_, w| {
                            w.psize().variant(psize);
                            w.msize().variant(msize)
                        });
                    }

                    /// Set the priority level of this channel
                    pub fn set_priority_level(&mut self, priority: Priority) {
                        use pac::dma1::ch::cr::PL_A;
                        let pl = match priority {
                            Priority::Low => PL_A::LOW,
                            Priority::Medium => PL_A::MEDIUM,
                            Priority::High => PL_A::HIGH,
                            Priority::VeryHigh => PL_A::VERYHIGH,
                        };
                        self.ch().cr.modify(|_, w| w.pl().variant(pl));
                    }

                    /// Enable circular mode for this channel
                    pub fn enable_circular_mode(&mut self) {
                        self.ch().cr.modify(|_, w| w.circ().enabled());
                    }

                    /// Start a read transfer (peripheral to memory)
                    pub fn start_read(&mut self) {
                        self.ch().cr.modify(|_, w| {
                            w.dir().from_peripheral();
                            w.en().enabled()
                        });
                    }

                    /// Start a write transfer (memory to peripheral)
                    pub fn start_write(&mut self) {
                        self.ch().cr.modify(|_, w| {
                            w.dir().from_memory();
                            w.en().enabled()
                        });
                    }

                    /// Stop the current transfer
                    pub fn stop(&mut self) {
                        self.ch().cr.modify(|_, w| w.en().disabled());
                    }

                    /// Is the current transfer complete?
                    pub fn is_transfer_complete(&self) -> bool {
                        read_isr().$tcifi().bit_is_set()
                    }

                    /// Is the first half of the current transfer complete?
                    pub fn is_transfer_half_done(&self) -> bool {
                        read_isr().$htifi().bit_is_set()
                    }

                    /// Clear the 'transfer complete' flag
                    pub fn clear_transfer_complete(&mut self) {
                        // NOTE(unsafe) atomic write to a stateless register
                        unsafe {
                            ifcr().write(|w| w.$ctcifi().set_bit());
                        }
                    }

                    /// Clear the 'half transfer' flag
                    pub fn clear_half_transfer(&mut self) {
                        // NOTE(unsafe) atomic write to a stateless register
                        unsafe {
                            ifcr().write(|w| w.$chtifi().set_bit());
                        }
                    }

                    /// Enable the interrupt for the given event
                    pub fn listen(&mut self, event: Event) {
                        match event {
                            Event::HalfTransfer => {
                                self.ch().cr.modify(|_, w| w.htie().enabled());
                            }
                            Event::TransferComplete => {
                                self.ch().cr.modify(|_, w| w.tcie().enabled());
                            }
                        }
                    }
                }

                impl<B, P> Transfer<B, $Ci, P> {
                    /// Is this transfer complete?
                    pub fn is_complete(&self) -> bool {
                        self.channel.is_transfer_complete()
                    }

                    /// Wait until the transfer is done and return ownership
                    /// over its parts.
                    pub fn wait(mut self) -> (&'static mut B, $Ci, P)
                    where
                        P: TransferPayload,
                    {
                        while !self.is_complete() {}

                        atomic::compiler_fence(Ordering::SeqCst);

                        self.payload.stop();
                        self.channel.stop();

                        (self.buf, self.channel, self.payload)
                    }
                }

                impl <B, P> CircularTransfer<B, $Ci, P> {
                    /// Call the given function on the readable half of the
                    /// DMA buffer
                    pub fn peek<F, R>(&mut self, f: F) -> Result<R, Error>
                    where
                        F: FnOnce(&B) -> R,
                    {
                        self.update_readable_half()?;

                        let half = match self.readable_half {
                            Half::First => &self.buf[0],
                            Half::Second => &self.buf[1],
                        };

                        let result = f(half);

                        match self.readable_half {
                            Half::First if self.second_half_done() => Err(Error::Overrun),
                            Half::Second if self.first_half_done() => Err(Error::Overrun),
                            _ => Ok(result),
                        }
                    }

                    fn first_half_done(&self) -> bool {
                        self.channel.is_transfer_half_done()
                    }

                    fn second_half_done(&self) -> bool {
                        self.channel.is_transfer_complete()
                    }

                    fn clear_first_half_done(&mut self) {
                        self.channel.clear_half_transfer();
                    }

                    fn clear_second_half_done(&mut self) {
                        self.channel.clear_transfer_complete();
                    }

                    fn update_readable_half(&mut self) -> Result<(), Error> {
                        let first_done = self.first_half_done();
                        let second_done = self.second_half_done();

                        if first_done && second_done {
                            return Err(Error::Overrun);
                        }

                        match self.readable_half {
                            Half::First if second_done => {
                                self.clear_second_half_done();
                                self.readable_half = Half::Second;
                            }
                            Half::Second if first_done => {
                                self.clear_first_half_done();
                                self.readable_half = Half::First;
                            }
                            _ => (),
                        }
                        Ok(())
                    }
                }
            )+
        }
    };

    (
        controller: $x:expr,
        channels: [ $( $i:expr ),+ ],
    ) => {
        paste::item! {
            dma! {
                controller: ([<DMA $x>], [<dma $x>]),
                channels: [
                    $( ([<C $i>], [<ch $i>],
                        [<tcif $i>], [<htif $i>], [<ctcif $i>], [<chtif $i>]), )+
                ],
            }
        }
    };
}

dma! {
    controller: 1,
    channels: [1, 2, 3, 4, 5, 6, 7],
}

dma! {
    controller: 2,
    channels: [1, 2, 3, 4, 5],
}
